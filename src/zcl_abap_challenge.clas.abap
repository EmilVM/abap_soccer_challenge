CLASS zcl_abap_challenge DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    INTERFACES: if_oo_adt_classrun, zif_abap_challenge.

    "Defining the positions that a player can have, and store them in a specific value
    CONSTANTS: BEGIN OF gc_player_position,
                 goalkeeper TYPE i VALUE 1,
                 defender   TYPE i VALUE 2,
                 midfielder TYPE i VALUE 3,
                 attacker   TYPE i VALUE 4,
               END OF gc_player_position.

    "Defining the person entity
    TYPES: BEGIN OF ty_person,
             person_id TYPE sysuuid_x16,
             name      TYPE string,
             club      TYPE sysuuid_x16,
           END OF ty_person.

    TYPES: tt_person TYPE STANDARD TABLE OF ty_person.

    "Defining the player entity and its properties
    TYPES: BEGIN OF ty_player,
             person     TYPE ty_person,
             age        TYPE i,
             player_pos TYPE i,
             red_cards  TYPE i,
             yel_cards  TYPE i,
             skill_lvl  TYPE i,
           END OF ty_player.

    TYPES: tt_players TYPE STANDARD TABLE OF ty_player.

    "Defining the club entity and its properties
    TYPES: BEGIN OF ty_club,
             club_id  TYPE sysuuid_x16,
             clubname TYPE string,
             city     TYPE string,
             country  TYPE string,
           END OF ty_club.

    TYPES: tt_clubs TYPE STANDARD TABLE OF ty_club.

    "Defining the match entity and its properties
    TYPES: BEGIN OF ty_match,
             match_id        TYPE sysuuid_x16,
             home_team       TYPE string,
             away_team       TYPE string,
             home_team_score TYPE i,
             away_team_score TYPE i,
           END OF ty_match.

    TYPES: tt_matches TYPE STANDARD TABLE OF ty_match.

    TYPES: tt_string_table TYPE STANDARD TABLE OF string.

    METHODS: constructor IMPORTING io_data_defaults TYPE REF TO zif_abap_challenge OPTIONAL,
      get_soccer_model IMPORTING iv_regenerate TYPE abap_bool DEFAULT abap_false
                       EXPORTING et_players    TYPE tt_players
                                 et_clubs      TYPE tt_clubs
                                 et_matches    TYPE tt_matches
                                 et_scorers    TYPE tt_person.

  PROTECTED SECTION.
  PRIVATE SECTION.
    "Defining the constants for all attributes, such as max age, skill level etc.
    CONSTANTS gc_age_min TYPE i VALUE 18.
    CONSTANTS gc_age_max TYPE i VALUE 40.
    CONSTANTS gc_skill_min TYPE i VALUE 20.
    CONSTANTS gc_skill_max TYPE i VALUE 100.
    CONSTANTS gc_cards_min TYPE i VALUE 0.
    CONSTANTS gc_cards_max TYPE i VALUE 2.
    CONSTANTS gc_score_min TYPE i VALUE 0.
    CONSTANTS gc_score_max TYPE i VALUE 3.
    CONSTANTS gc_no_clubs_min TYPE i VALUE 4.
    CONSTANTS gc_no_clubs_max TYPE i VALUE 16.
    CONSTANTS gc_no_player_min TYPE i VALUE 7.
    CONSTANTS gc_no_player_max TYPE i VALUE 19.
    CONSTANTS gc_min_no_matches TYPE i VALUE 40.
    CONSTANTS gc_max_no_matches TYPE i VALUE 200.

    "Variable declaration
    DATA: go_data_defaults    TYPE REF TO zif_abap_challenge,
          gt_players          TYPE tt_players,
          gt_clubs            TYPE tt_clubs,
          gt_matches          TYPE tt_matches,
          gt_scorers          TYPE tt_person,
          gt_firstnames       TYPE zif_abap_challenge~tt_string_table,
          gt_lastnames        TYPE zif_abap_challenge~tt_string_table,
          gt_clubnames        TYPE zif_abap_challenge~tt_string_table,
          gt_citynames        TYPE zif_abap_challenge~tt_cities,
          gt_countrynames     TYPE zif_abap_challenge~tt_contries,
          go_default_random   TYPE REF TO cl_abap_random_int,
          go_club_random      TYPE REF TO cl_abap_random_int,
          go_city_random      TYPE REF TO cl_abap_random_int,
          go_country_random   TYPE REF TO cl_abap_random_int,
          go_exc_random       TYPE REF TO cx_abap_random,
          go_firstname_random TYPE REF TO cl_abap_random_int,
          go_lastname_random  TYPE REF TO cl_abap_random_int,
          go_age_random       TYPE REF TO cl_abap_random_int,
          go_player_random    TYPE REF TO cl_abap_random_int,
          go_playerpos_random TYPE REF TO cl_abap_random_int,
          go_skill_random     TYPE REF TO cl_abap_random_int,
          go_cards_random     TYPE REF TO cl_abap_random_int.

    METHODS:
      get_new_uuid RETURNING VALUE(rv_uuid) TYPE sysuuid_x16,
      clear_data,
      generate_clubs,
      generate_clubnames,
      generate_citynames,
      generate_countrynames,
      generate_matches,
      get_random_country RETURNING VALUE(rv_country) TYPE zif_abap_challenge~ty_country,
      get_random_city RETURNING VALUE(rv_city) TYPE zif_abap_challenge~ty_city,
      get_random_clubname IMPORTING iv_delete_assigned TYPE abap_bool RETURNING VALUE(rv_clubname) TYPE string,
      add_scorer IMPORTING iv_club TYPE sysuuid_x16 iv_score TYPE i,
      generate_playernames,
      generate_players,
      get_player_name RETURNING VALUE(rv_name) TYPE string,
      get_random_age RETURNING VALUE(rv_age) TYPE i,
      get_random_player_pos RETURNING VALUE(rv_playerpos) TYPE i,
      get_random_skill_lvl RETURNING VALUE(rv_skill) TYPE i,
      get_random_cards RETURNING VALUE(rv_cards) TYPE i.
ENDCLASS.

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

CLASS zcl_abap_challenge IMPLEMENTATION.

  METHOD clear_data.
    CLEAR: gt_matches[], gt_players[], gt_scorers[], gt_clubs[].
  ENDMETHOD.

  METHOD constructor.
    IF io_data_defaults IS NOT BOUND.
      go_data_defaults = io_data_defaults.
    ENDIF.
    "Create a Default Randomizer, in case of Randomizer Exceptions
    go_default_random = cl_abap_random_int=>create( min = 0 max = 0 ).
  ENDMETHOD.

  """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  METHOD generate_citynames.
    DATA: lt_cities TYPE zif_abap_challenge~tt_cities.
    CLEAR gt_citynames[].
    IF me->gt_countrynames IS INITIAL.
      me->generate_countrynames( ).
    ENDIF.
    LOOP AT me->gt_countrynames ASSIGNING FIELD-SYMBOL(<fs_country>).
      CLEAR lt_cities[].
      go_data_defaults->get_city_names( EXPORTING iv_country_id = <fs_country>-country_id IMPORTING et_citynames = lt_cities ).
      APPEND LINES OF lt_cities TO gt_citynames.
    ENDLOOP.
  ENDMETHOD.


  METHOD generate_countrynames.
    CLEAR gt_countrynames[].
    go_data_defaults->get_country_names( IMPORTING et_countrynames = gt_countrynames ).
  ENDMETHOD.


  METHOD generate_matches.
    DATA: ls_match        TYPE ty_match,
          lo_random_team  TYPE REF TO cl_abap_random_int,
          lo_random_score TYPE REF TO cl_abap_random_int.

    TRY.
        lo_random_team = cl_abap_random_int=>create( min = 1 max = lines( gt_clubs ) ).
        lo_random_score = cl_abap_random_int=>create( min = gc_score_min max = gc_score_max ).
      CATCH cx_abap_random INTO go_exc_random.
        lo_random_team = go_default_random.
        lo_random_score = go_default_random.
    ENDTRY.

    TRY.
        DO cl_abap_random_int=>create( min = gc_min_no_matches max = gc_max_no_matches )->get_next( ) TIMES.

          "Counters
          DATA(lv_home_team) = lo_random_team->get_next( ).
          DATA(lv_away_team) = lo_random_team->get_next( ).
          IF lv_home_team = lv_away_team.
            CONTINUE.
          ENDIF.

          "Match Setup
          ls_match-match_id = get_new_uuid( ).
          ls_match-home_team = gt_clubs[ lv_home_team ]-clubname.
          ls_match-away_team = gt_clubs[ lv_away_team ]-clubname.
          ls_match-home_team_score = lo_random_score->get_next( ).
          ls_match-away_team_score = lo_random_score->get_next( ).
          me->add_scorer( iv_club = gt_clubs[ lv_home_team ]-club_id iv_score = ls_match-home_team_score ).
          me->add_scorer( iv_club = gt_clubs[ lv_away_team ]-club_id iv_score = ls_match-away_team_score ).
          APPEND ls_match TO gt_matches.
        ENDDO.
      CATCH cx_abap_random INTO go_exc_random.
        "Do Nothing - No matches are returned.
    ENDTRY.
  ENDMETHOD.


  METHOD add_scorer.
    DATA: lv_score_remaining TYPE i,
          lv_number_clubs    TYPE i,
          lt_current_club    TYPE tt_players.

    IF iv_score > gc_score_min.

      lv_score_remaining = iv_score.
      lt_current_club[] = gt_players[].
      DELETE lt_current_club WHERE person-club NE iv_club.

      TRY.
          DATA(lo_player) = cl_abap_random_int=>create( min = 1 max = lines( lt_current_club ) ).
        CATCH cx_abap_random INTO go_exc_random.
          lo_player = go_default_random.
      ENDTRY.

      WHILE lv_score_remaining > gc_score_min.
        APPEND lt_current_club[ lo_player->get_next( ) ]-person TO gt_scorers.
        lv_score_remaining = lv_score_remaining - 1.
      ENDWHILE.
    ENDIF.
  ENDMETHOD.


  METHOD generate_playernames.
    CLEAR: gt_firstnames[], gt_lastnames[].
    go_data_defaults->get_firstnames( IMPORTING et_firstnames = gt_firstnames ).
    go_data_defaults->get_lastnames( IMPORTING et_lastnames = gt_lastnames ).
  ENDMETHOD.

  METHOD generate_players.
    DATA: ls_player TYPE ty_player.

    IF go_player_random IS NOT BOUND.
      TRY.
          go_player_random = cl_abap_random_int=>create( min = gc_no_player_min max = gc_no_player_max ).
        CATCH cx_abap_random INTO go_exc_random.
          go_player_random = go_default_random.
      ENDTRY.
    ENDIF.

    LOOP AT me->gt_clubs ASSIGNING FIELD-SYMBOL(<fs_club>).
      DO go_player_random->get_next( ) TIMES.
        CLEAR ls_player.
        ls_player-person-person_id = get_new_uuid( ).
        ls_player-age = get_random_age( ).
        ls_player-player_pos = get_random_player_pos( ).
        ls_player-skill_lvl = get_random_skill_lvl( ).
        ls_player-red_cards = get_random_cards( ).
        ls_player-yel_cards = get_random_cards( ).
        ls_player-person-club = <fs_club>-club_id.
        ls_player-person-name = get_player_name( ).
        APPEND ls_player TO gt_players.
      ENDDO.
    ENDLOOP.
  ENDMETHOD.


  METHOD generate_clubnames.
    CLEAR gt_clubnames[].
    go_data_defaults->get_clubnames( IMPORTING et_clubnames = gt_clubnames ).
  ENDMETHOD.


  METHOD generate_clubs.
    DATA: ls_club TYPE ty_club.

    CLEAR me->gt_clubs[].

    IF go_club_random IS NOT BOUND.
      TRY.
          go_club_random = cl_abap_random_int=>create( min = gc_no_clubs_min max = gc_no_clubs_max ).
        CATCH cx_abap_random INTO go_exc_random.
          go_club_random = go_default_random.
      ENDTRY.
    ENDIF.

    DO go_club_random->get_next( ) TIMES.
      CLEAR ls_club.
      ls_club-club_id = get_new_uuid( ).
      ls_club-clubname = get_random_clubname( abap_true ).
      ls_club-city = get_random_city( )-city.
      ls_club-country = get_random_country( )-country_txt.
      APPEND ls_club TO gt_clubs.
    ENDDO.
  ENDMETHOD.


  """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

  METHOD get_new_uuid.
    TRY.
        rv_uuid = cl_system_uuid=>if_system_uuid_static~create_uuid_x16( ).
      CATCH cx_uuid_error INTO DATA(lo_exception).
        rv_uuid = 0.
    ENDTRY.
  ENDMETHOD.


  METHOD get_player_name.
    IF gt_firstnames IS INITIAL OR gt_lastnames IS INITIAL.
      me->generate_playernames( ).
    ENDIF.

    IF go_firstname_random IS NOT BOUND.
      TRY.
          go_firstname_random = cl_abap_random_int=>create( min = 1 max = lines( gt_firstnames ) ).
        CATCH cx_abap_random INTO go_exc_random.
          go_firstname_random = go_default_random.
      ENDTRY.
    ENDIF.

    IF go_lastname_random IS NOT BOUND.
      TRY.
          go_lastname_random = cl_abap_random_int=>create( min = 1 max = lines( gt_lastnames ) ).
        CATCH cx_abap_random INTO go_exc_random.
          go_lastname_random = go_default_random.
      ENDTRY.
    ENDIF.

    DATA(lv_firstname) = gt_firstnames[ go_firstname_random->get_next( ) ].
    DATA(lv_lastname) = gt_lastnames[ go_lastname_random->get_next( ) ].

    CONCATENATE lv_firstname lv_lastname INTO rv_name SEPARATED BY space.
  ENDMETHOD.


  METHOD get_random_age.
    IF go_age_random IS NOT BOUND.
      TRY.
          go_age_random = cl_abap_random_int=>create( min = gc_age_min max = gc_age_max ).
        CATCH cx_abap_random INTO go_exc_random.
          go_age_random = go_default_random.
      ENDTRY.
    ENDIF.
    rv_age = go_age_random->get_next( ).
  ENDMETHOD.


  METHOD get_random_player_pos.
    IF go_playerpos_random IS NOT BOUND.
      TRY.
          go_playerpos_random = cl_abap_random_int=>create( min = gc_player_position-goalkeeper max = gc_player_position-attacker ).
        CATCH cx_abap_random INTO go_exc_random.
          go_playerpos_random = go_default_random.
      ENDTRY.
    ENDIF.
    rv_playerpos = go_playerpos_random->get_next( ).
  ENDMETHOD.


  METHOD get_random_skill_lvl.
    IF go_skill_random IS NOT BOUND.
      TRY.
          go_skill_random = cl_abap_random_int=>create( min = gc_skill_min max = gc_skill_max ).
        CATCH cx_abap_random INTO go_exc_random.
          go_skill_random = go_default_random.
      ENDTRY.
    ENDIF.
    rv_skill = go_skill_random->get_next( ).
  ENDMETHOD.


  METHOD get_random_cards.
    IF go_cards_random IS NOT BOUND.
      TRY.
          go_cards_random = cl_abap_random_int=>create( min = gc_cards_min max = gc_cards_max ).
        CATCH cx_abap_random INTO go_exc_random.
          go_cards_random = go_default_random.
      ENDTRY.
    ENDIF.
    rv_cards = go_cards_random->get_next( ).
  ENDMETHOD.


  METHOD get_random_city.
    IF gt_citynames IS INITIAL.
      me->generate_citynames( ).
    ENDIF.

    IF go_city_random IS NOT BOUND.
      TRY.
          go_city_random = cl_abap_random_int=>create( min = 1 max = lines( gt_citynames ) ).
        CATCH cx_abap_random INTO go_exc_random.
          go_city_random = go_default_random.
      ENDTRY.
    ENDIF.
    rv_city = gt_citynames[ go_city_random->get_next( ) ].
  ENDMETHOD.


  METHOD get_random_country.
    IF gt_countrynames IS INITIAL.
      me->generate_countrynames( ).
    ENDIF.

    IF go_country_random IS NOT BOUND.
      TRY.
          go_country_random = cl_abap_random_int=>create( min = 1 max = lines( gt_countrynames ) ).
        CATCH cx_abap_random INTO go_exc_random.
          go_country_random = go_default_random.
      ENDTRY.
    ENDIF.
    DATA(lv_country_no) = go_country_random->get_next( ).

    rv_country = gt_countrynames[ lv_country_no ].
  ENDMETHOD.


  METHOD get_random_clubname.
    IF gt_clubnames IS INITIAL.
      me->generate_clubnames(  ).
    ENDIF.

    TRY.
        DATA(lv_club_no) = cl_abap_random_int=>create( min = 1 max = lines( gt_clubnames ) )->get_next(  ).
      CATCH cx_abap_random INTO go_exc_random.
    ENDTRY.

    TRY.
        rv_clubname = gt_clubnames[ lv_club_no ].
        DELETE gt_clubnames INDEX lv_club_no.
      CATCH cx_root.
        "Do Nothing!
    ENDTRY.
  ENDMETHOD.


  METHOD get_soccer_model.
    IF go_data_defaults IS NOT BOUND.
      go_data_defaults = me."Default
    ENDIF.

    IF iv_regenerate EQ abap_true.
      me->clear_data( ).
    ENDIF.

    IF me->gt_clubs IS INITIAL.
      me->generate_clubs(  ).
    ENDIF.
    et_clubs[] = me->gt_clubs[].

    IF me->gt_players IS INITIAL.
      me->generate_players( ).
    ENDIF.
    et_players[] = me->gt_players[].

    IF me->gt_matches IS INITIAL AND me->gt_scorers IS INITIAL.
      me->generate_matches( ).
    ENDIF.
    et_matches[] = me->gt_matches[].
    et_scorers[] = me->gt_scorers[].
  ENDMETHOD.

  """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  METHOD if_oo_adt_classrun~main.

    me->get_soccer_model(
    IMPORTING
        et_players = DATA(lt_players)
        et_clubs   = DATA(lt_clubs)
        et_matches = DATA(lt_matches)
        et_scorers = DATA(lt_scorers)
    ).

    "Write to the console
    out->write(
        EXPORTING
            data = lt_clubs
            name = 'Clubs'
    ).
    out->write( '______________________________________________________ ' ).

    out->write(
        EXPORTING
            data = lt_players
            name = 'Players'
    ).
    out->write( '______________________________________________________ ' ).

    out->write(
        EXPORTING
            data = lt_matches
            name = 'Matches'
    ).
    out->write( '______________________________________________________ ' ).

    out->write(
        EXPORTING
            data = lt_scorers
            name = 'Scorers'
    ).
  ENDMETHOD.

  """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

  METHOD zif_abap_challenge~get_city_names.
    et_citynames = VALUE #( ( country_id = iv_country_id city = 'Pear Village' )
                            ( country_id = iv_country_id city = 'Apple Town' )
                            ( country_id = iv_country_id city = 'Orange Village' )
                            ( country_id = iv_country_id city = 'Strawbarry City' )
                            ( country_id = iv_country_id city = 'Potato Town' ) ).
  ENDMETHOD.

  METHOD zif_abap_challenge~get_country_names.
    et_countrynames = VALUE #( ( country_id = 'DK' country_txt = 'Denmark' )
                               ( country_id = 'SE' country_txt = 'Sweden' )
                               ( country_id = 'NO' country_txt = 'Norway' )
                               ( country_id = 'RU' country_txt = 'Russia' )
                               ( country_id = 'EN' country_txt = 'England' )
                               ( country_id = 'DE' country_txt = 'Germany' )
                               ( country_id = 'GR' country_txt = 'Greece' )
                               ( country_id = 'FR' country_txt = 'France' )
                               ( country_id = 'NL' country_txt = 'Holland' )
                               ( country_id = 'BE' country_txt = 'Belgium' )
                               ( country_id = 'ES' country_txt = 'Spain' )
                               ( country_id = 'IT' country_txt = 'Italy' ) ).
  ENDMETHOD.

  METHOD zif_abap_challenge~get_firstnames.
    APPEND 'Thomas' TO et_firstnames.
    APPEND 'Robin' TO et_firstnames.
    APPEND 'Ian' TO et_firstnames.
    APPEND 'Ann' TO et_firstnames.
    APPEND 'Lee' TO et_firstnames.
    APPEND 'Martin' TO et_firstnames.
    APPEND 'Rita' TO et_firstnames.
    APPEND 'Jakob' TO et_firstnames.
    APPEND 'Hubba' TO et_firstnames.
    APPEND 'Milo' TO et_firstnames.
    APPEND 'Egon' TO et_firstnames.
    APPEND 'Shannon' TO et_firstnames.
    APPEND 'Leon' TO et_firstnames.
    APPEND 'Donald' TO et_firstnames.
    APPEND 'Allie' TO et_firstnames.
    APPEND 'Morgan' TO et_firstnames.
    APPEND 'Sarah' TO et_firstnames.
    APPEND 'Bill' TO et_firstnames.
    APPEND 'Harrod' TO et_firstnames.
    APPEND 'Vernon' TO et_firstnames.
    APPEND 'Eve' TO et_firstnames.
    APPEND 'Adam' TO et_firstnames.
    APPEND 'Minnie' TO et_firstnames.
    APPEND 'Gareth' TO et_firstnames.
    APPEND 'Lily' TO et_firstnames.
    APPEND 'Tara' TO et_firstnames.
    APPEND 'Little' TO et_firstnames.
    APPEND 'Vera' TO et_firstnames.
    APPEND 'Poul' TO et_firstnames.
    APPEND 'Allen' TO et_firstnames.
    APPEND 'Bob' TO et_firstnames.
    APPEND 'Hugo' TO et_firstnames.
    APPEND 'Victor' TO et_firstnames.
    APPEND 'Red' TO et_firstnames.
    APPEND 'Robert' TO et_firstnames.
    APPEND 'Lille Lise' TO et_firstnames.
    APPEND 'Ron' TO et_firstnames.
    APPEND 'Mad' TO et_firstnames.
    APPEND 'Thunder' TO et_firstnames.
    APPEND 'Isaac' TO et_firstnames.
  ENDMETHOD.

  METHOD zif_abap_challenge~get_lastnames.
    APPEND 'McCarthur' TO et_lastnames.
    APPEND 'Stuart' TO et_lastnames.
    APPEND 'Wright' TO et_lastnames.
    APPEND 'Connelly' TO et_lastnames.
    APPEND 'Anderson' TO et_lastnames.
    APPEND 'Jensen' TO et_lastnames.
    APPEND 'Poulsen' TO et_lastnames.
    APPEND 'McDonalds' TO et_lastnames.
    APPEND 'Howard' TO et_lastnames.
    APPEND 'De La Vega' TO et_lastnames.
    APPEND 'Rivaldino' TO et_lastnames.
    APPEND 'Shaggy' TO et_lastnames.
    APPEND 'Rock' TO et_lastnames.
    APPEND 'Blast' TO et_lastnames.
    APPEND 'Illinois' TO et_lastnames.
    APPEND 'Queen' TO et_lastnames.
    APPEND 'Trump' TO et_lastnames.
    APPEND 'King' TO et_lastnames.
    APPEND 'Kardashian' TO et_lastnames.
    APPEND 'Cerentina' TO et_lastnames.
    APPEND 'Allensby' TO et_lastnames.
    APPEND 'Jordan' TO et_lastnames.
    APPEND 'La Vista' TO et_lastnames.
    APPEND 'Zulu' TO et_lastnames.
    APPEND 'Peewee' TO et_lastnames.
    APPEND 'Hope' TO et_lastnames.
    APPEND 'Morgan' TO et_lastnames.
    APPEND 'Garfield' TO et_lastnames.
    APPEND 'Little' TO et_lastnames.
    APPEND 'Huge' TO et_lastnames.
    APPEND 'Sizemore' TO et_lastnames.
    APPEND 'Bond' TO et_lastnames.
    APPEND 'Maximillian' TO et_lastnames.
    APPEND 'Kuyasakka' TO et_lastnames.
    APPEND 'Ignatio' TO et_lastnames.
    APPEND 'McStuffin' TO et_lastnames.
    APPEND 'Drummond' TO et_lastnames.
    APPEND 'Clarkson' TO et_lastnames.
    APPEND 'May' TO et_lastnames.
    APPEND 'Rex' TO et_lastnames.
    APPEND 'Wild' TO et_lastnames.
    APPEND 'Thunder' TO et_lastnames.
    APPEND 'La Playa' TO et_lastnames.
    APPEND 'Hasta La Vista' TO et_lastnames.
    APPEND 'Mogensen' TO et_lastnames.
    APPEND 'Junior' TO et_lastnames.
    APPEND 'Senior' TO et_lastnames.
  ENDMETHOD.

  METHOD zif_abap_challenge~get_clubnames.
    APPEND 'Lollipops' TO et_clubnames.
    APPEND 'Flanksteaks' TO et_clubnames.
    APPEND 'Marooners' TO et_clubnames.
    APPEND 'Purple Power' TO et_clubnames.
    APPEND 'Honkidonk' TO et_clubnames.
    APPEND 'Massive Balls' TO et_clubnames.
    APPEND 'Allen Wrentches' TO et_clubnames.
    APPEND 'No Intentions' TO et_clubnames.
    APPEND 'Herpies' TO et_clubnames.
    APPEND 'Lagoons' TO et_clubnames.
  ENDMETHOD.
ENDCLASS.
