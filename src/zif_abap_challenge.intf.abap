INTERFACE zif_abap_challenge
  PUBLIC .

  TYPES: tt_string_table TYPE STANDARD TABLE OF string.

  TYPES: BEGIN OF ty_country,
           country_id  TYPE land1,
           country_txt TYPE string,
         END OF ty_country.

  TYPES: tt_contries TYPE STANDARD TABLE OF ty_country.

  TYPES: BEGIN OF ty_city,
           country_id TYPE land1,
           city       TYPE string,
         END OF ty_city.

  TYPES: tt_cities TYPE STANDARD TABLE OF ty_city.

  METHODS:
    get_firstnames EXPORTING et_firstnames TYPE tt_string_table,
    get_lastnames EXPORTING et_lastnames TYPE tt_string_table,
    get_clubnames EXPORTING et_clubnames TYPE tt_string_table,
    get_city_names IMPORTING iv_country_id TYPE land1
                   EXPORTING et_citynames  TYPE tt_cities,
    get_country_names EXPORTING et_countrynames TYPE tt_contries.

ENDINTERFACE.
