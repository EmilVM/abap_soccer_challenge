# ABAP Soccer Challenge
A challenge project developed for the Graduate Program at Gavdi Labs Aalborg.
Purpose was to create a Data Model with auto-generated Data in ABAP, with the ability to output the content of the Data Model using the Console.
For this project, the topic of Soccer was used, and the program aims to generate and simulate matches between different soccer teams. 
---
## Requirements
For this project the requirements for the data model was as follows:

  - A Player must have Name and an Age and belong to a Club. 
  - A Club must have a City and a Country, plus a Set of Players.
  - A Match consists of two Clubs and a set of goals, based on the Home- and Away Team's Score.
  - It must show who scores which goals.
  - Negative scores are not allowed.
  - A Club cannot have a match against themselves.
  - The number of Players in a Club needs to be between 7 and 19. 
  - A Players has a Player Type, which is either a GoalKeeper, Defender, Midfielder, or an Attacker. 
  - A Player cannot have more than two cards (red or yellow).
  - A Player has a Skill level between 20 and 100.
---
## Installation
- Install this project via [ABAPGit](http://abapgit.org).